set names utf8;
set foreign_key_checks = 0;

drop database if exists logindb;
create database logindb;
use logindb;

create table user(
user_id int(11),
user_name varchar(255),
password varchar(255)
);

insert into user values(1, "tanaka", "tanaka"), (2, "suzuki", "suzuki"), (3, "nakamura", "nakamura");