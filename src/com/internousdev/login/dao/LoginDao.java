package com.internousdev.login.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.internousdev.login.dto.*;
import com.internousdev.login.util.DBConnector;

public class LoginDao {
	public LoginDto select(String user_name, String password) throws SQLException{
		LoginDto dto = new LoginDto();
		DBConnector db = new DBConnector();
		Connection con =  db.getConnection();

		String sql ="SELECT * FROM user WHERE user_name=? and password=?";

		try {
			//セキュリティを考慮しているらしい
			//何が起きているかあとで調べる
			PreparedStatement ps = con.prepareStatement(sql);
			//sqlのパラメータに指定した値が入ってくる
			ps.setString(1, user_name);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				//DBから取得した内容をセット
				dto.setUserName(rs.getString("user_name"));
				dto.setPassword(rs.getString("password"));
			}
		}catch(SQLException e) {
			//例外出力
			e.printStackTrace();
		}finally {
			//DB切断
			con.close();
		}
		return dto;
	}
}
