package com.internousdev.login.action;

import java.sql.SQLException;
import java.util.Map;
import org.apache.struts2.interceptor.SessionAware;
import com.opensymphony.xwork2.ActionSupport;

import com.internousdev.login.dao.*;
import com.internousdev.login.dto.*;

public class LoginAction extends ActionSupport implements SessionAware{

	private String name;
	private String password;
	private Map<String, Object> session;

	public String execute() throws SQLException{
		String res = "error";
		LoginDao dao = new LoginDao();
		LoginDto dto = new LoginDto();

		dto = dao.select(name, password);
		if(name.equals(dto.getUserName())){
			if(password.equals(dto.getPassword())) {
				res = "success";
			}
		}
		session.put("name", dto.getUserName());
		System.out.println(res);
		return res;
	}

	public String getName() {
		return name;
	}

	public void setName(String user_name) {
		this.name = user_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Map<String, Object> getSession(){
		return session;
	}

	public void setSession(Map<String, Object> session){
		this.session = session;
	}

}
